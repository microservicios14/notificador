##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: telegram_controller.py
# Capitulo: Estilo Microservicios
# Autor(es): Perla Velasco & Yonathan Mtz. & Jorge Solís
# Version: 3.0.0 Febrero 2022
# Descripción:
#
#   Ésta clase define el controlador del microservicio API. 
#   Implementa la funcionalidad y lógica de negocio del Microservicio.
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |     send_message()     |         Ninguno          |  - Procesa el mensaje |
#           |                        |                          |    recibido en la     |
#           |                        |                          |    petición y ejecuta |
#           |                        |                          |    el envío a         |
#           |                        |                          |    Telegram.          |
#           +------------------------+--------------------------+-----------------------+
#
#-------------------------------------------------------------------------
from flask import request, jsonify
import json, time
import telepot
import requests
from src.helpers import config

class TelegramController:

    @staticmethod
    def send_message():
        data = json.loads(request.data)
        if not data:
            return jsonify({"msg": "invalid request"}), 400
        #Codigo creado por Eladio
        message = f"mensaje {data['message']}"
        
        conf = config.load_config()
        token = conf['TELEGRAM']['TOKEN']
        chat_id = conf['TELEGRAM']['CHAT_ID']
        bot = telepot.Bot(token)
        ##Código creado por Ricardo
        urlRuta=f"{data['rutaFile']}"
        resp = requests.get(urlRuta, timeout=None)
        open('/tmp/policy.pdf','wb').write(resp.content)
        doc = open('/tmp/policy.pdf','rb')
        #doc = request.files [urlRuta]
        bot.sendMessage(chat_id, message)
        bot.sendDocument(chat_id,doc)
        time.sleep(1)
        return jsonify({"msg": "se envio la notificacion"}), 200
